package io.jumpco.demo.jumpcobuilddemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JumpcoBuildDemoApplicationTests {

	@Test
	public void contextLoads() {
	}

}
