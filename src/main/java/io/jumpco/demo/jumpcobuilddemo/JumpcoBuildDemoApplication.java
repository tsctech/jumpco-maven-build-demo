package io.jumpco.demo.jumpcobuilddemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JumpcoBuildDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JumpcoBuildDemoApplication.class, args);
	}
}
