#!/usr/bin/env bash
if [ "${MAVEN_USERNAME}" == "" ]
then
    echo "Expected ENV VAR: MAVEN_USERNAME"
    exit 1
fi
if [ "${MAVEN_PASSWORD}" == "" ]
then
    echo "Expected ENV VAR: MAVEN_PASSWORD"
    exit 1
fi
mkdir -p ~/.m2
echo "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\
<settings xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\
        xmlns=\"http://maven.apache.org/SETTINGS/1.0.0\"\
        xsi:schemaLocation=\"http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd\">\
    <servers>\
		<server>\
			<id>bintray-jumpco-jumpco-maven</id>\
			<username>${MAVEN_USERNAME}</username>\
            <password>${MAVEN_PASSWORD}</password>\
		</server>\
	</servers>\
</settings>" > ~/.m2/settings.xml
echo "Applied Maven Configuration"